https://www.tutorialspoint.com/sqlalchemy/sqlalchemy_orm_creating_session.htm
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind = engine)
session = Session()

Some of the frequently required methods of session class are listed below −

Sr.No.	Method & Description
1
begin()

begins a transaction on this session

2
add()

places an object in the session. Its state is persisted in the database on next flush operation

3
add_all()

adds a collection of objects to the session

4
commit()

flushes all items and any transaction in progress

5
delete()

marks a transaction as deleted

6
execute()

executes a SQL expression

7
expire()

marks attributes of an instance as out of date

8
flush()

flushes all object changes to the database

9
invalidate()

closes the session using connection invalidation

10
rollback()

rolls back the current transaction in progress

11
close()

Closes current session by clearing all items and ending any transaction in progress

>>> from db import db_conn
>>> from models.books import Book
>>> result = db_conn.session.query(Book)
>>> result
<flask_sqlalchemy.BaseQuery object at 0x7fb58b7a6dd8>
>>> result.statement
<sqlalchemy.sql.selectable.Select at 0x7fb58b7a6cc0; Select object>
>>> str(result.statement)
'SELECT book.title \nFROM book'
>>> 
