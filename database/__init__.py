from config import app
from flask_sqlalchemy import SQLAlchemy
db_con_uri = 'mysql://root:password@localhost/book_management'
app.config["SQLALCHEMY_DATABASE_URI"] = db_con_uri
db = SQLAlchemy(app)
