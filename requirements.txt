Flask==1.1.2
Flask-SQLAlchemy==2.4.4
mysqlclient==2.0.3
gunicorn==20.0.0
celery==4.3.0
redis==3.3.11
Flask-Script==2.0.6