from config import app
from database import db
from flask import render_template, request, redirect
from models.books import Book
from models.users import User
from tasks import welcome, hello_task

app.debug = True


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/celery_task', methods=["GET", "POST"])
def celery_task():
    print("celery_task called")
    task_id_w = welcome.delay()
    print("welcome task id: ", task_id_w)
    task_id_h = hello_task.delay()
    print("hello task id: ", task_id_h)
    return 'Hello in the world of Python Flask! Vinay'


@app.route('/user', methods=['GET', 'POST'])
def user():
    if request.form:
        print(request.form)
        user_obj = User()
        email = request.form.get("email")
        f_name = request.form.get("fname")
        l_name = request.form.get("lname")
        user_obj.email = email
        user_obj.first_name = f_name
        user_obj.last_name = l_name
        db.session.add(user_obj)
        db.session.commit()

    return render_template('user.html')


@app.route("/home", methods=["GET", "POST"])
def home():
    if request.form:
        print(request.form)
        book = Book(title=request.form.get("title"))
        db.session.add(book)
        db.session.commit()

    all_books = Book.query.filter()
    return render_template("home.html", books=tuple(all_books))


@app.route("/update", methods=["POST"])
def update():
    new_title = request.form.get("newtitle")
    old_title = request.form.get("oldtitle")
    book = Book.query.filter_by(title=old_title).first()
    book.title = new_title
    db.session.commit()
    return redirect("/home")
    # return 'success'


@app.route("/delete", methods=["POST"])
def delete():
    title = request.form.get("title")
    book = Book.query.filter_by(title=title).first()
    db.session.delete(book)
    db.session.commit()
    return redirect("/")
    # return 'success'


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5001)
    app.debug(True)
