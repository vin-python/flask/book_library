import time
from celery import Celery
import logging

CELERY_BROKER_URL = 'redis://localhost:6379'
app = Celery('tasks', broker=CELERY_BROKER_URL)

logging.basicConfig(
    filename="test.log",
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(name)-15s %(message)s"
)


# create logger
logger = logging.getLogger("logging")
logger.setLevel(logging.DEBUG)
# create console handler and set level to debug
output_file_handler = logging.FileHandler("logging_output.log")
stdout_handler = logging.StreamHandler()
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# add formatter to stdout_handler, output_file_handler
stdout_handler.setFormatter(formatter)
output_file_handler.setFormatter(formatter)
logger.addHandler(output_file_handler)
logger.addHandler(stdout_handler)


# @shared_task
# @app.task
@app.task
def welcome():
    print("Welcome  task is executed")
    logger.debug("Welcome  task is executed")
    for i in range(50):
        time.sleep(.5)
        print("Welcome loop count: " + str(i))
        logger.debug("Welcome loop count: " + str(i))
    print("Welcome End")
    logger.debug("Welcome End")
    # return multiplication # it doesn't make any sense in real


@app.task
def hello_task():
    print("hello  task is executed")
    logger.debug("hello  task is executed")
    for i in range(50):
        time.sleep(.1)
        print("hello loop count: " + str(i))
        logger.debug("hello loop count: " + str(i))
    print("hello End")
    logger.debug("hello End")
    # return multiplication # it doesn't make any sense in real
