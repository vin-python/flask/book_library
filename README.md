python3.6
MySQL5.6

# virtualenv -p python3.6 venv
# create table inside database


--------------------
Run flask app

1: 
    export FLASK_ENV=development
    flask run
    
2:
    export FLASK_APP=app
    flask run
    
3:
    python nameof_app_file.py
    
    if __name__ == "__main__":
        app.run(host='127.0.0.1', port=5002)
        app.debug(True)

SETUP
_________________________
  
>>> from models.books import db
>>> db.create_all()
>>> exit()


https://www.digitalocean.com/community/tutorials/how-to-structure-large-flask-applications


_________________________________
MIGRATION

    https://flask-migrate.readthedocs.io/en/latest/
    flask db init
    flask db migrate
    flask db upgrade
    flask db --help