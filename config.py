# Define the application directory
import os
from flask import Flask

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
