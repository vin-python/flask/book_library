
---------------------
Flask deployment
----------------------
https://medium.com/ymedialabs-innovation/deploy-flask-app-with-nginx-using-gunicorn-and-supervisor-d7a93aa07c18
supervisor
-------------------------------
We will be using the following technologies:
Flask: Server backend
Nginx: Reverse proxy
Gunicorn: Deploy flask app
Supervisor: Monitor and control gunicorn process




PATH=/etc/supervisor/conf.d/my_flask_app.conf
[program:my_flask_app]
directory=/var/www/my_flask_app
command=/var/www/my_flask_app/env/bin/gunicorn app:app -b localhost:5000
autostart=true  
autorestart=true
stderr_logfile=/var/log/my_flask_app/error_my_flask_app.log
stdout_logfile=/var/log/my_flask_app/access_my_flask_app.log

sudo supervisorctl reread
sudo service supervisor restart
sudo supervisorctl status



nginx
------------------------------
sudo nano  /etc/nginx/conf.d/my_app_flask_virtual.conf

http://127.0.0.1:81/

$ sudo nginx -t
$ sudo service nginx restart

If code changes in Flask App, restart supervisor

Flask, Celery, Redis
============================
celery -A tasks worker --loglevel=info